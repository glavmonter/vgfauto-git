#include <iostream>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->plotCooling->Configure();
    ui->plotHeating->Configure();

    m_pTimerSettings = new QTimer();
    m_pTimerSettings->setSingleShot(true);
    m_pTimerSettings->setInterval(2000);
    connect(m_pTimerSettings, &QTimer::timeout, this, &MainWindow::saveSettings);

    /* Mapper для переключения вкладок на ToolBoxMode */
    m_pSignalMapperModeToolBox = new QSignalMapper(this);
    m_pSignalMapperModeToolBox->setMapping(ui->radioModeHeating, 0);
    m_pSignalMapperModeToolBox->setMapping(ui->radioModeCooling, 1);
    m_pSignalMapperModeToolBox->setMapping(ui->radioModeGradHeating, 2);
    m_pSignalMapperModeToolBox->setMapping(ui->radioModeGradCooling, 3);
    connect(ui->radioModeHeating, SIGNAL(clicked()), m_pSignalMapperModeToolBox, SLOT(map()));
    connect(ui->radioModeCooling, SIGNAL(clicked()), m_pSignalMapperModeToolBox, SLOT(map()));
    connect(ui->radioModeGradHeating, SIGNAL(clicked()), m_pSignalMapperModeToolBox, SLOT(map()));
    connect(ui->radioModeGradCooling, SIGNAL(clicked()), m_pSignalMapperModeToolBox, SLOT(map()));
    connect(m_pSignalMapperModeToolBox, SIGNAL(mapped(int)), ui->toolBoxMode, SLOT(setCurrentIndex(int)));


    /* Mapper для выбора текущего положения радиобаттонов режима */
    m_pSignalMapperMode = new QSignalMapper(this);
    m_pSignalMapperMode->setMapping(ui->radioModeNone, Thermo::Automatization_AutoMode_MODE_NONE);
    m_pSignalMapperMode->setMapping(ui->radioModeHeating, Thermo::Automatization_AutoMode_MODE_PREHEATING);
    m_pSignalMapperMode->setMapping(ui->radioModeCooling, Thermo::Automatization_AutoMode_MODE_COOLING);
    m_pSignalMapperMode->setMapping(ui->radioModeGradCooling, Thermo::Automatization_AutoMode_MODE_GRADIENT_COOLING);
    connect(ui->radioModeNone, SIGNAL(clicked()), m_pSignalMapperMode, SLOT(map()));
    connect(ui->radioModeHeating, SIGNAL(clicked()), m_pSignalMapperMode, SLOT(map()));
    connect(ui->radioModeCooling, SIGNAL(clicked()), m_pSignalMapperMode, SLOT(map()));
    connect(ui->radioModeGradCooling, SIGNAL(clicked()), m_pSignalMapperMode, SLOT(map()));
    connect(m_pSignalMapperMode, SIGNAL(mapped(int)), this, SLOT(ModeChanged(int)));
    ModeChanged(Thermo::Automatization_AutoMode_MODE_NONE);

    connect(ui->btnConnect, &QPushButton::clicked, this, &MainWindow::slotConnectDisconnect);
    connect(ui->btnStartStop, &QPushButton::clicked, this, &MainWindow::slotStartStop);

    connect(ui->spinGrowthHighT, SIGNAL(valueChanged(double)), ui->plotGradientCooling, SLOT(setHighT(double)));
    connect(ui->spinGrowthHighT, SIGNAL(editingFinished()), m_pTimerSettings, SLOT(start()));

    connect(ui->spinGrowthMeltT, SIGNAL(valueChanged(double)), ui->plotGradientCooling, SLOT(setMeltT(double)));
    connect(ui->spinGrowthMeltT, SIGNAL(editingFinished()), m_pTimerSettings, SLOT(start()));

    connect(ui->spinGrowthLowT, SIGNAL(valueChanged(double)), ui->plotGradientCooling, SLOT(setLowT(double)));
    connect(ui->spinGrowthLowT, SIGNAL(editingFinished()), m_pTimerSettings, SLOT(start()));

    connect(ui->spinGrowthGradientT, SIGNAL(valueChanged(double)), ui->plotGradientCooling, SLOT(setGradient(double)));
    connect(ui->spinGrowthGradientT, SIGNAL(editingFinished()), m_pTimerSettings, SLOT(start()));

    connect(ui->sliderGrowthPositionX, SIGNAL(valueChanged(int)), ui->plotGradientCooling, SLOT(setXPos(int)));
    connect(ui->sliderGrowthPositionX, SIGNAL(valueChanged(int)), this, SLOT(showPosition(int)));

QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
QRegExp ipRegex("^" + ipRange
                    + "\\." + ipRange
                    + "\\." + ipRange
                    + "\\." + ipRange + "$");

    ui->editIP->setValidator(new QRegExpValidator(ipRegex, this));

QSettings settings;
    ui->editIP->setText(settings.value("address", "127.0.0.1").toString());

    ui->btnConnect->setText(tr("Connect"));
    ui->btnStartStop->setText(tr("Start"));

    settings.beginGroup("gradient_cooling");
        ui->spinGrowthHighT->setValue(settings.value("hight", 1120.0).toDouble());
        ui->spinGrowthMeltT->setValue(settings.value("meltt", 1095.0).toDouble());
        ui->spinGrowthLowT->setValue(settings.value("lowt", 1080.0).toDouble());
        ui->spinGrowthGradientT->setValue(settings.value("gradient", 1.0).toDouble());
    settings.endGroup();

    settings.beginGroup("preheating");
        ui->spinHeatingStartT->setValue(settings.value("startt", 100.0).toDouble());
        ui->spinHeatingStopT->setValue(settings.value("stopt", 800.0).toDouble());
        ui->spinHeatingSpeed->setValue(settings.value("speed", 100.0).toDouble());
    settings.endGroup();

    settings.beginGroup("cooling");
        ui->spinCoolingStartT->setValue(settings.value("startt", 800.0).toDouble());
        ui->spinCoolingStopT->setValue(settings.value("stopt", 100.0).toDouble());
        ui->spinCoolingSpeed->setValue(settings.value("speed", 100.0).toDouble());
    settings.endGroup();


    ui->plotGradientCooling->setHighT(ui->spinGrowthHighT->value());
    ui->plotGradientCooling->setMeltT(ui->spinGrowthMeltT->value());
    ui->plotGradientCooling->setLowT(ui->spinGrowthLowT->value());
    ui->plotGradientCooling->setGradient(ui->spinGrowthGradientT->value());


    m_pTcpSocket = new QTcpSocket();
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotTcpError(QAbstractSocket::SocketError)));
    connect(m_pTcpSocket, SIGNAL(readyRead()), this, SLOT(slotTcpReadyRead()));
    connect(m_pTcpSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(slotTcpStateChanged(QAbstractSocket::SocketState)));
}

MainWindow::~MainWindow()
{
    saveSettings();
    delete ui;
}


void MainWindow::slotConnectDisconnect()
{
    if (m_bConnected == false) {
        /* Тут еще все отключено, включаюсь */
        m_bConnected = true;
        connectPressed();
    } else {
        /* Тут все работает, выключаюсь */
        m_bConnected = false;
        disconnectPressed();
        stopPressed();
    }
}


void MainWindow::slotStartStop()
{
    if (m_bStarted == false) {
        startPressed();
    } else {
        stopPressed();
    }
}

void MainWindow::showPosition(int pos)
{
double p = pos/1000.0;
ui->labelPosition->setText(tr("%1 mm").arg(QString::number(p, 'f', 2)));
}

void MainWindow::ModeChanged(int mode)
{
QString str;
    switch(mode) {
    case Thermo::Automatization_AutoMode_MODE_NONE:
        str = "NONE";
        m_xMode = Thermo::Automatization_AutoMode_MODE_NONE;
        break;

    case Thermo::Automatization_AutoMode_MODE_PREHEATING:
        str = "PREHEATING";
        m_xMode = Thermo::Automatization_AutoMode_MODE_PREHEATING;
        break;

    case Thermo::Automatization_AutoMode_MODE_COOLING:
        str = "COOLING";
        m_xMode = Thermo::Automatization_AutoMode_MODE_COOLING;
        break;

    case Thermo::Automatization_AutoMode_MODE_GRADIENT_COOLING:
        str = "GRADIENT_COOLING";
        m_xMode = Thermo::Automatization_AutoMode_MODE_GRADIENT_COOLING;
        break;

    default:
        m_xMode = Thermo::Automatization_AutoMode_MODE_NONE;
    }


    qDebug() << tr("Mode changed: %1").arg(str);
}

void MainWindow::saveSettings()
{
QSettings settings;

    settings.beginGroup("gradient_cooling");
        settings.setValue("hight", ui->spinGrowthHighT->value());
        settings.setValue("meltt", ui->spinGrowthMeltT->value());
        settings.setValue("lowt", ui->spinGrowthLowT->value());
        settings.setValue("gradient", ui->spinGrowthGradientT->value());
    settings.endGroup();

    settings.beginGroup("preheating");
        settings.setValue("startt", ui->spinHeatingStartT->value());
        settings.setValue("stopt", ui->spinHeatingStopT->value());
        settings.setValue("speed", ui->spinHeatingSpeed->value());
    settings.endGroup();

    settings.beginGroup("cooling");
        settings.setValue("startt", ui->spinCoolingStartT->value());
        settings.setValue("stopt", ui->spinCoolingStopT->value());
        settings.setValue("speed", ui->spinCoolingSpeed->value());
    settings.endGroup();
}


void MainWindow::startPressed()
{
    m_bStarted = true;
    ui->btnStartStop->setText(tr("Stop"));
}


void MainWindow::stopPressed()
{
    m_bStarted = false;
    ui->btnStartStop->setText(tr("Start"));
}


void MainWindow::connectPressed()
{
    ui->btnConnect->setText(tr("Disconnect"));
    ui->editIP->setEnabled(false);
    ui->btnStartStop->setEnabled(true);

    m_pTcpSocket->connectToHost(ui->editIP->text(), 9999);
    if (m_pTcpSocket->waitForConnected(5000) == false) {
        disconnectPressed();
        return;
    }

TCP::ClientServer cts;
    cts.set_type(TCP::ClientServer_ClientType_CLIENT_LOCAL);
    std::string str = cts.SerializePartialAsString();
    m_pTcpSocket->write(str.data(), str.size());
}

void MainWindow::disconnectPressed()
{
    m_pTcpSocket->disconnectFromHost();
    ui->btnConnect->setText(tr("Connect"));
    ui->editIP->setEnabled(true);
    ui->btnStartStop->setEnabled(false);
}

void MainWindow::on_btnSend_clicked()
{
    if (m_pTcpSocket->state() != QAbstractSocket::ConnectedState) {
        QMessageBox::information(this, tr("Not connected"), tr("Client not connected to VGFServer.\n"
                                                               "Please, connect first"));
        return;
    }

    qDebug() << tr("Sending data to server");

TCP::ClientServer cts;
Thermo::Payload payload;
    payload.set_deviceid(1);

    for (int i = 0; i < 22; i++) {
        Thermo::HostToDevice *htd = payload.add_htd();
        Thermo::Automatization *auto_mode = new Thermo::Automatization;
        auto_mode->set_mode(m_xMode);

        if (m_xMode == Thermo::Automatization_AutoMode_MODE_PREHEATING) {
            Thermo::Automatization_HeatCool *heat = new Thermo::Automatization_HeatCool;
            heat->set_temperaturestart(ui->spinHeatingStartT->value());
            heat->set_temperaturestop(ui->spinHeatingStopT->value());
            heat->set_speed(ui->spinHeatingSpeed->value()/3600.0);

            auto_mode->set_allocated_heatcooldata(heat);

        } else if (m_xMode == Thermo::Automatization_AutoMode_MODE_COOLING) {
            Thermo::Automatization_HeatCool *cool = new Thermo::Automatization_HeatCool;
            cool->set_temperaturestart(ui->spinCoolingStartT->value());
            cool->set_temperaturestop(ui->spinCoolingStopT->value());
            cool->set_speed(ui->spinCoolingSpeed->value()/3600.0);

            auto_mode->set_allocated_heatcooldata(cool);

        } else if (m_xMode == Thermo::Automatization_AutoMode_MODE_GRADIENT_COOLING) {
            Thermo::Automatization_Growing *growth = new Thermo::Automatization_Growing;
            growth->set_temperaturehigh(ui->spinGrowthHighT->value());
            growth->set_temperaturemelt(ui->spinGrowthMeltT->value());
            growth->set_gradient(ui->spinGrowthGradientT->value());
            growth->set_speed(ui->spinGrowthSpeed->value()/3600.0); // mm/s
            growth->set_currentpos(ui->sliderGrowthPositionX->value()/1000.0);

            auto_mode->set_allocated_growingdata(growth);
        }

        htd->set_allocated_auto_(auto_mode);
        htd->set_zonenumber(i);
    }

    cts.set_type(TCP::ClientServer_ClientType_CLIENT_LOCAL);
    cts.set_setdata(TCP::SET_CLIENT_SETTINGS);
    cts.set_thermo(payload.SerializeAsString());

    std::string str = cts.SerializeAsString();
    m_pTcpSocket->write(str.data(), str.size());
}


void MainWindow::slotTcpStateChanged(QAbstractSocket::SocketState socketState)
{
    if (socketState == QAbstractSocket::ConnectedState) {
        ui->btnSend->setEnabled(true);
    } else {
        ui->btnSend->setEnabled(false);
    }
}

void MainWindow::slotTcpError(QAbstractSocket::SocketError)
{
    QMessageBox::critical(this, tr("Network error"), m_pTcpSocket->errorString());
    disconnectPressed();
}

void MainWindow::slotTcpReadyRead()
{

}
