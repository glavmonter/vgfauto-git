
#include "plotfurnacegrad.h"

#include <qwt_plot_grid.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_directpainter.h>
#include <qwt_curve_fitter.h>
#include <qwt_painter.h>
#include <qwt_plot_picker.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_legenditem.h>
#include <qwt_symbol.h>

#include <QEvent>
#include <QString>

//#include "customplotpicker.h"

class LegendItem: public QwtPlotLegendItem
{
public:
    LegendItem()
    {
        setRenderHint( QwtPlotItem::RenderAntialiased );

        QColor color( Qt::white );

        setTextPen( color );
#if 1
        setBorderPen( color );

        QColor c( Qt::gray );
        c.setAlpha( 200 );

        setBackgroundBrush( c );
#endif
    }
};


PlotFurnaceGrad::PlotFurnaceGrad(QWidget *parent):
    QwtPlot(parent),
    verticalInterval(0.0, 240.0)
{
    setAutoReplot(false);

    //canvas()->setPaintAttribute(QwtPlotCanvas::BackingStore, false);

    initGradient();

    plotLayout()->setAlignCanvasToScales(true);

    setAxisTitle(QwtPlot::xBottom, tr("Zone"));
    setAxisScale(QwtPlot::xBottom, 0.51, 22.4, 1.0);
    setAxisTitle(QwtPlot::yLeft, tr("Temperature [°C]"));
    setAxisScale(QwtPlot::yLeft, verticalInterval.minValue(), verticalInterval.maxValue());

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(QPen(Qt::green, 0.0, Qt::DotLine));
    grid->enableX(true);
    grid->enableXMin(false);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);

    InitData();

    QwtPlotPicker *picker = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft, QwtPlotPicker::CrossRubberBand,
                                             QwtPicker::ActiveOnly, this->canvas());

    picker->setRubberBandPen(QColor(Qt::red));
    picker->setTrackerPen(QColor(Qt::blue));
    picker->setStateMachine(new QwtPickerDragPointMachine());



LegendItem *legendItem = new LegendItem();
    legendItem->attach(this);


    curvePresetT = new QwtPlotCurve();
    curvePresetT->setStyle(QwtPlotCurve::Lines);
    curvePresetT->setPen(QPen(Qt::green, 2));
    curvePresetT->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    curvePresetT->setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    curvePresetT->setRawSamples(ZonePosition.data(), PresetTemps.data(), PresetTemps.size());
    curvePresetT->setTitle(tr("Preset"));
    curvePresetT->attach(this);

QwtSymbol *symbol1 = new QwtSymbol(QwtSymbol::Ellipse, QBrush(Qt::yellow), QPen(Qt::red, 2), QSize(8, 8));
    curvePresetT->setSymbol(symbol1);


    curveCurrentT = new QwtPlotCurve();
    curveCurrentT->setStyle(QwtPlotCurve::Lines);
    curveCurrentT->setPen(QPen(Qt::red, 2));
    curveCurrentT->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    curveCurrentT->setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    curveCurrentT->setRawSamples(ZonePosition.data(), CurrentTemps.data(), CurrentTemps.size());
    curveCurrentT->setTitle(tr("Current"));
    curveCurrentT->attach(this);

QwtSymbol *symbol2 = new QwtSymbol(QwtSymbol::Ellipse, QBrush(Qt::yellow), QPen(Qt::red, 2), QSize(8, 8));
    curveCurrentT->setSymbol(symbol2);


    markerHighT = new QwtPlotMarker();
    markerHighT->setLineStyle(QwtPlotMarker::HLine);
    markerHighT->setLinePen(QPen(Qt::magenta, 2.0, Qt::DashLine));
    markerHighT->setYValue(m_dHighT);
    markerHighT->setLabelAlignment(Qt::AlignRight);
    markerHighT->attach(this);


    markerMeltT = new QwtPlotMarker();
    markerMeltT->setLineStyle(QwtPlotMarker::HLine);
    markerMeltT->setLinePen(QPen(Qt::darkCyan, 2.0, Qt::DashLine));
    markerMeltT->setYAxis(m_dMeltT);
    markerMeltT->attach(this);


    markerLowT = new QwtPlotMarker();
    markerLowT->setLineStyle(QwtPlotMarker::HLine);
    markerLowT->setLinePen(QPen(Qt::magenta, 2.0, Qt::DashLine));
    markerLowT->setYValue(m_dLowT);
    markerLowT->attach(this);
}

PlotFurnaceGrad::~PlotFurnaceGrad()
{

}



void PlotFurnaceGrad::updateAll()
{
    updateVerticalInterval();
    replot();
}


void PlotFurnaceGrad::setPresetT(QVector<double> &T)
{
    for (int i = 0; i < T.size(); i++)
        PresetTemps[i] = T[i];

    updateVerticalInterval();
}


void PlotFurnaceGrad::setCurrentT(QVector<double> &T)
{
    for (int i = 0; i < T.size(); i++)
        CurrentTemps[i] = T[i];
}


void PlotFurnaceGrad::setHighT(double T)
{
    m_dHighT = T;
    Calculate();
}


void PlotFurnaceGrad::setMeltT(double T)
{
    m_dMeltT = T;
    Calculate();
}


void PlotFurnaceGrad::setLowT(double T)
{
    m_dLowT = T;
    Calculate();
}

void PlotFurnaceGrad::setGradient(double grad)
{
    m_dGradient = grad;
    Calculate();
}

void PlotFurnaceGrad::setXPos(int x)
{
    setXPos(x/1000.0);
}

void PlotFurnaceGrad::setXPos(double x)
{
    m_dPositionX = x;
    Calculate();
}


void PlotFurnaceGrad::Calculate()
{
    markerHighT->setYValue(m_dHighT);
    markerLowT->setYValue(m_dLowT);
    markerMeltT->setYValue(m_dMeltT);

#define ZONE_SIZE           10  // mm

    for (int i = 0; i < PresetTemps.size(); i++) {
        double t = m_dGradient*10.0/ZONE_SIZE*((i + 1)*ZONE_SIZE - m_dPositionX) + m_dMeltT;
        if (t > m_dHighT)
            t = m_dHighT;

        PresetTemps[i] = t;
    }

    updateVerticalInterval();
    replot();
}


void PlotFurnaceGrad::updateVerticalInterval()
{
    verticalInterval.setInterval(m_dLowT - 10, m_dHighT + 10);
    setAxisScale(QwtPlot::yLeft, verticalInterval.minValue(), verticalInterval.maxValue());
    replot();
}


void PlotFurnaceGrad::InitData()
{
const int num_zones = 22;
double dx = 1.0;
double x = 1.0;

    for (int i = 0; i < num_zones; i++) {
        ZonePosition.append(x);
        PresetTemps.append(28.0);
        CurrentTemps.append(24.0);
        x += dx;
    }
}


void PlotFurnaceGrad::initGradient()
{
    QPalette pal = canvas()->palette();

    QLinearGradient gradient( 0.0, 0.0, 1.0, 0.0 );
    gradient.setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient.setColorAt(0.0, QColor( 0, 49, 110 ) );
    gradient.setColorAt(1.0, QColor( 0, 87, 174 ) );

    pal.setBrush(QPalette::Window, QBrush(gradient));

    canvas()->setPalette(pal);
}


void PlotFurnaceGrad::setIntervalLength(double interval)
{
    setIntervalLength(interval);
}


void PlotFurnaceGrad::updateCurve()
{

}
