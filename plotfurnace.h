#ifndef PLOTFURNACE_H
#define PLOTFURNACE_H

#include <qwt_plot.h>
#include <qwt_interval.h>
#include <qwt_system_clock.h>

#include <QFile>

class QwtPlotCurve;
class QwtPlotMarker;

class PlotFurnace: public QwtPlot
{
    Q_OBJECT

public:
    PlotFurnace(QWidget * = NULL);
    ~PlotFurnace();

    double getPresetT(int zone);
    double getCurrentT(int zone);
    double getAmplouleT(int zone);

    void Configure(bool showPresetT = false, bool showPicker = false, bool showSymbols = false, bool showLegend = false);


public slots:
    void setIntervalLength(double);

    void setPresetT(int zoneIndex, double T);
    void setCurrentT(int zoneIndex, double T);
    void setAmplouleT(int zoneIndex, double T);

    void updateAll();

    void setPresetT(QVector<double> &T);
    void setCurrentT(QVector<double> &T);

private:
    void initGradient();
    void updateCurve();

    void InitData();
    void updateVerticalInterval();

    QwtInterval verticalInterval;
    QVector <double> ZonePosition;
    QVector <double> AmpoulePosition;
    QVector <double> PresetTemps;
    QVector <double> CurrentTemps;
    QVector <double> AmpouleTemps;

    QwtPlotCurve *curveCurrentT;
    QwtPlotCurve *curvePresetT;
    QwtPlotCurve *curveAmpouleT;
};

#endif // PLOTFURNACE_H
