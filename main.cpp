#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setOrganizationName("MUCTR");
    QApplication::setOrganizationDomain("muctr.ru");
    QApplication::setApplicationName("VGFAutomatization");

    QTranslator translator;
    QString trName = "://lang/VGFAutomatization_" + QLocale::system().name() + ".qm";
    translator.load(trName);
    a.installTranslator(&translator);

    MainWindow w;
    w.show();

    return a.exec();
}
