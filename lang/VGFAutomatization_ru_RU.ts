<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>VGF Automatization</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="42"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="48"/>
        <location filename="../mainwindow.ui" line="133"/>
        <source>Heating</source>
        <translation>Нагрев</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="58"/>
        <location filename="../mainwindow.ui" line="271"/>
        <source>Cooling</source>
        <translation>Охлаждение</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="65"/>
        <location filename="../mainwindow.ui" line="409"/>
        <source>Gradient heating</source>
        <translation>Нагрев с градиентом</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="72"/>
        <location filename="../mainwindow.ui" line="422"/>
        <source>Gradient cooling</source>
        <translation>Охлаждение с градиентом</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="90"/>
        <source>Start_Stop</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="148"/>
        <location filename="../mainwindow.ui" line="286"/>
        <source>Start T:</source>
        <translation>Начало:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="155"/>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Stop T:</source>
        <translation>Окончание:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="162"/>
        <location filename="../mainwindow.ui" line="300"/>
        <source>Speed:</source>
        <translation>Скорость:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="169"/>
        <location filename="../mainwindow.ui" line="188"/>
        <location filename="../mainwindow.ui" line="307"/>
        <location filename="../mainwindow.ui" line="326"/>
        <source> °C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="207"/>
        <location filename="../mainwindow.ui" line="345"/>
        <source> °C/h</source>
        <translation> °C/ч</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="220"/>
        <location filename="../mainwindow.ui" line="358"/>
        <source>Time remaining:</source>
        <translation>До окончания:</translation>
    </message>
</context>
<context>
    <name>PlotFurnace</name>
    <message>
        <location filename="../plotfurnace.cpp" line="56"/>
        <source>Zone</source>
        <translation>Зона</translation>
    </message>
    <message>
        <location filename="../plotfurnace.cpp" line="58"/>
        <source>Temperature [°C]</source>
        <translation>Температура [°C]</translation>
    </message>
    <message>
        <location filename="../plotfurnace.cpp" line="101"/>
        <source>Preset</source>
        <translation>Уставка</translation>
    </message>
    <message>
        <location filename="../plotfurnace.cpp" line="120"/>
        <source>Current</source>
        <translation>Текущая</translation>
    </message>
</context>
</TS>
