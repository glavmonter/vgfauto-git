#-------------------------------------------------
#
# Project created by QtCreator 2015-05-29T10:37:17
#
#-------------------------------------------------

QT       += core gui network svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VGFAutomatization
TEMPLATE = app
CONFIG += debug_and_release c++11


SOURCES += main.cpp\
        mainwindow.cpp\
        plotfurnace.cpp\
        plotfurnacegrad.cpp

HEADERS  += mainwindow.h\
        plotfurnace.h\
        plotfurnacegrad.h

FORMS    += mainwindow.ui


TRANSLATIONS += \
            lang/VGFAutomatization_ru_RU.ts


PROTOS +=   ../protos/protocol_tcp.proto \
            ../protos/VGFThermo.proto \
            ../protos/VGFPlatinum.proto

include (protobuf.pri)

RESOURCES += VGFAutomatization.qrc


unix {
    LIBS += -lprotobuf

    INCLUDEPATH += /usr/include/qwt6-qt5/
    LIBS += -lqwt-qt5
}
