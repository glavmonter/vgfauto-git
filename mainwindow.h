#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSignalMapper>
#include <QSettings>
#include <QDebug>
#include <QTimer>
#include <QTcpSocket>
#include <QMessageBox>

#include "protocol_tcp.pb.h"
#include "VGFThermo.pb.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QSignalMapper *m_pSignalMapperModeToolBox;
    QSignalMapper *m_pSignalMapperMode;
    Thermo::Automatization_AutoMode m_xMode;

    bool m_bStarted = false;
    bool m_bConnected = false;

    QTimer *m_pTimerSettings;
    QTcpSocket *m_pTcpSocket;

    void startPressed();
    void stopPressed();
    void connectPressed();
    void disconnectPressed();

private slots:
    void slotConnectDisconnect();
    void slotStartStop();
    void showPosition(int pos);
    void ModeChanged(int mode);

    void slotTcpError(QAbstractSocket::SocketError);
    void slotTcpStateChanged(QAbstractSocket::SocketState socketState);

    void slotTcpReadyRead();


    void saveSettings();
    void on_btnSend_clicked();
};

#endif // MAINWINDOW_H
