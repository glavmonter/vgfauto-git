
#include "plotfurnace.h"

#include <qwt_plot_grid.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_directpainter.h>
#include <qwt_curve_fitter.h>
#include <qwt_painter.h>
#include <qwt_plot_picker.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_legenditem.h>
#include <qwt_symbol.h>

#include <QEvent>
#include <QString>

//#include "customplotpicker.h"

class LegendItem: public QwtPlotLegendItem
{
public:
    LegendItem()
    {
        setRenderHint( QwtPlotItem::RenderAntialiased );

        QColor color( Qt::white );

        setTextPen( color );
#if 1
        setBorderPen( color );

        QColor c( Qt::gray );
        c.setAlpha( 200 );

        setBackgroundBrush( c );
#endif
    }
};


PlotFurnace::PlotFurnace(QWidget *parent):
    QwtPlot(parent),
    verticalInterval(0.0, 240.0)
{
    setAutoReplot(false);

    //canvas()->setPaintAttribute(QwtPlotCanvas::BackingStore, false);

    initGradient();

    plotLayout()->setAlignCanvasToScales(true);

    setAxisTitle(QwtPlot::xBottom, tr("Zone"));
    setAxisScale(QwtPlot::xBottom, 0.51, 22.4, 1.0);
    setAxisTitle(QwtPlot::yLeft, tr("Temperature [°C]"));
    setAxisScale(QwtPlot::yLeft, verticalInterval.minValue(), verticalInterval.maxValue());

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(QPen(Qt::green, 0.0, Qt::DotLine));
    grid->enableX(true);
    grid->enableXMin(false);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);

    InitData();

//    CustomPlotPicker *picker = new CustomPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft, QwtPlotPicker::CrossRubberBand,
//                                             QwtPicker::ActiveOnly, this->canvas());

//    picker->setRubberBandPen(QColor(Qt::red));
//    picker->setTrackerPen(QColor(Qt::green));
//    picker->setStateMachine(new QwtPickerDragPointMachine());
//    picker->setPlotData(ZonePosition.data(), CurrentTemps.data(), PresetTemps.data(), PresetTemps.size());
}

PlotFurnace::~PlotFurnace()
{

}

void PlotFurnace::Configure(bool showPresetT, bool showPicker, bool showSymbols, bool showLegend)
{
Q_UNUSED(showPicker);

    if (showLegend) {
        LegendItem *legendItem = new LegendItem();
        legendItem->attach(this);
    }


    curvePresetT = new QwtPlotCurve();
    curvePresetT->setStyle(QwtPlotCurve::Lines);
    curvePresetT->setPen(QPen(Qt::green, 2));
    curvePresetT->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    curvePresetT->setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    curvePresetT->setRawSamples(ZonePosition.data(), PresetTemps.data(), PresetTemps.size());
    curvePresetT->setTitle(tr("Preset"));

    if (showPresetT)
        curvePresetT->attach(this);

    if (showSymbols) {
        QwtSymbol *symbol1 = new QwtSymbol(QwtSymbol::Ellipse, QBrush(Qt::yellow), QPen(Qt::red, 2), QSize(8, 8));
        curvePresetT->setSymbol(symbol1);
    }

    curveCurrentT = new QwtPlotCurve();
    curveCurrentT->setStyle(QwtPlotCurve::Lines);
    curveCurrentT->setPen(QPen(Qt::red, 2));
    curveCurrentT->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    curveCurrentT->setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    curveCurrentT->setRawSamples(ZonePosition.data(), CurrentTemps.data(), CurrentTemps.size());
    curveCurrentT->setTitle(tr("Current"));
    curveCurrentT->attach(this);
    if (showSymbols) {
        QwtSymbol *symbol2 = new QwtSymbol(QwtSymbol::Ellipse, QBrush(Qt::yellow), QPen(Qt::red, 2), QSize(8, 8));
        curveCurrentT->setSymbol(symbol2);
    }

}

void PlotFurnace::setPresetT(int zoneIndex, double T)
{
    if (zoneIndex < PresetTemps.size()) {
        PresetTemps[zoneIndex] = T;
        updateVerticalInterval();
    }
}

void PlotFurnace::setCurrentT(int zoneIndex, double T)
{
    if (zoneIndex < CurrentTemps.size()) {
        CurrentTemps[zoneIndex] = T;
        updateVerticalInterval();
    }
}

void PlotFurnace::setAmplouleT(int zoneIndex, double T)
{
    if (zoneIndex < AmpouleTemps.size()) {
        AmpouleTemps[zoneIndex] = T;
        replot();
    }
}

void PlotFurnace::updateAll()
{
    updateVerticalInterval();
    replot();
}

void PlotFurnace::setPresetT(QVector<double> &T)
{
    for (int i = 0; i < T.size(); i++)
        PresetTemps[i] = T[i];

    updateVerticalInterval();
}

void PlotFurnace::setCurrentT(QVector<double> &T)
{
    for (int i = 0; i < T.size(); i++)
        CurrentTemps[i] = T[i];
}


double PlotFurnace::getPresetT(int zone)
{
    if (zone < PresetTemps.size())
        return PresetTemps[zone];

    return -1.0;
}


double PlotFurnace::getCurrentT(int zone)
{
    if (zone < CurrentTemps.size())
        return CurrentTemps[zone];

    return -1.0;
}

double PlotFurnace::getAmplouleT(int zone)
{
    if (zone < AmpouleTemps.size())
        return AmpouleTemps[zone];

    return -1.0;
}


void PlotFurnace::updateVerticalInterval()
{
QVector<double>::iterator max = std::max_element(CurrentTemps.begin(), CurrentTemps.end());
QVector<double>::iterator min = std::min_element(CurrentTemps.begin(), CurrentTemps.end());   

    verticalInterval.setInterval(*min - 40, *max + 40);
    setAxisScale(QwtPlot::yLeft, verticalInterval.minValue(), verticalInterval.maxValue());
    replot();
}


void PlotFurnace::InitData()
{
const int num_zones = 22;
double dx = 1.0;
double x = 1.0;

    for (int i = 0; i < num_zones; i++) {
        ZonePosition.append(x);
        PresetTemps.append(28.0);
        CurrentTemps.append(24.0);
        x += dx;
    }


    x = 18.0;
    dx = 48.0;
    for (int i = 0; i < 5; i++) {
        AmpoulePosition.append(x);
        AmpouleTemps.append(20.0);
        x += dx;
    }
}



void PlotFurnace::initGradient()
{
    QPalette pal = canvas()->palette();

    QLinearGradient gradient( 0.0, 0.0, 1.0, 0.0 );
    gradient.setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient.setColorAt(0.0, QColor( 0, 49, 110 ) );
    gradient.setColorAt(1.0, QColor( 0, 87, 174 ) );

    pal.setBrush(QPalette::Window, QBrush(gradient));

    canvas()->setPalette(pal);
}

void PlotFurnace::setIntervalLength(double interval)
{
    setIntervalLength(interval);
}

void PlotFurnace::updateCurve()
{

}
