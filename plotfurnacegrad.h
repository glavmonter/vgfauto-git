#ifndef PLOTFURNACEGRAD_H
#define PLOTFURNACEGRAD_H

#include <qwt_plot.h>
#include <qwt_interval.h>
#include <qwt_system_clock.h>

#include <QFile>

class QwtPlotCurve;
class QwtPlotMarker;

class PlotFurnaceGrad: public QwtPlot
{
    Q_OBJECT

public:
    PlotFurnaceGrad(QWidget * = NULL);
    ~PlotFurnaceGrad();

public slots:
    void setIntervalLength(double);

    void updateAll();

    void setPresetT(QVector<double> &T);
    void setCurrentT(QVector<double> &T);

    void setHighT(double T);
    void setMeltT(double T);
    void setLowT(double T);
    void setGradient(double grad);

    void setXPos(int x);
    void setXPos(double x);

    void Calculate();

private:
    void initGradient();
    void updateCurve();

    void InitData();
    void updateVerticalInterval();

    QwtInterval verticalInterval;
    QVector <double> ZonePosition;

    QVector <double> PresetTemps;
    QVector <double> CurrentTemps;

    QwtPlotCurve *curveCurrentT;
    QwtPlotCurve *curvePresetT;

    QwtPlotMarker *markerHighT;
    QwtPlotMarker *markerMeltT;
    QwtPlotMarker *markerLowT;

    double m_dHighT;
    double m_dMeltT;
    double m_dLowT;
    double m_dGradient;

    double m_dPositionX = 0.0;
};

#endif // PLOTFURNACEGRAD_H
